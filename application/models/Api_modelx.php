<?php
class Api_modelx extends CI_Model
{
	function fetch_all()
	{
		$this->db->order_by('id_siswa', 'DESC');
		return $this->db->get('ms_siswa');
	}

	function insert_api($data)
	{
		$this->db->insert('ms_siswa', $data);
	}

	function fetch_single_user($user_id)
	{
		$this->db->where('id_siswa', $user_id);
		$query = $this->db->get('ms_siswa');
		return $query->result_array();
	}

	function update_api($user_id, $data)
	{
		$this->db->where('id_siswa', $user_id);
		$this->db->update('ms_siswa', $data);
	}

	function delete_single_user($user_id)
	{
		$this->db->where('id_siswa', $user_id);
		$this->db->delete('ms_siswa');
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

?>