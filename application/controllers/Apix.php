<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apix extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('api_modelx');
		$this->load->library('form_validation');
	}

	function index()
	{
		$data = $this->api_modelx->fetch_all();
		echo json_encode($data->result_array());
	}

	function insert()
	{
		$this->form_validation->set_rules('nama_siswa', 'Nama Siswa', 'required');
		$this->form_validation->set_rules('nik_siswa', 'NIk Siswa', 'required');
		if($this->form_validation->run())
		{
			$data = array(
				'nama_siswa'	=>	$this->input->post('nama_siswa'),
				'nik_siswa'		=>	$this->input->post('nik_siswa')
			);

			$this->api_modelx->insert_api($data);

			$array = array(
				'success'		=>	true
			);
		}
		else
		{
			$array = array(
				'error'					=>	true,
				'nama_siswa_error'		=>	form_error('nama_siswa'),
				'nik_siswa_error'		=>	form_error('nik_siswa')
			);
		}
		echo json_encode($array);
	}
	
	function fetch_single()
	{
		if($this->input->post('id_siswa'))
		{
			$data = $this->api_modelx->fetch_single_user($this->input->post('id_siswa'));

			foreach($data as $row)
			{
				$output['nama_siswa'] = $row['nama_siswa'];
				$output['nik_siswa'] = $row['nik_siswa'];
			}
			echo json_encode($output);
		}
	}

	function update()
	{
		$this->form_validation->set_rules('nama_siswa', 'Nama Siswa', 'required');

		$this->form_validation->set_rules('nik_siswa', 'Nik Siswa', 'required');
		if($this->form_validation->run())
		{	
			$data = array(
				'nama_siswa'		=>	$this->input->post('nama_siswa'),
				'nik_siswa'			=>	$this->input->post('nik_siswa')
			);

			$this->api_modelx->update_api($this->input->post('id_siswa'), $data);

			$array = array(
				'success'		=>	true
			);
		}
		else
		{
			$array = array(
				'error'				=>	ture,
				'nama_siswa_error'	=>	form_error('nama_siswa'),
				'nik_siswa_error'	=>	form_error('nik_siswa')
			);
		}
		echo json_encode($array);
	}

	function delete()
	{
		if($this->input->post('id_siswa'))
		{
			if($this->api_modelx->delete_single_user($this->input->post('id_siswa')))
			{
				$array = array(

					'success'	=>	true
				);
			}
			else
			{
				$array = array(
					'error'		=>	true
				);
			}
			echo json_encode($array);
		}
	}

}


?>